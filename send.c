#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<string.h>
#include<libgen.h>
#include"send.h"

long sendbuffer(int sockfd, uint8_t *buffer, unsigned long bufferlen)
{
    unsigned long r, sent = 0;
    
    while(bufferlen > 0) {

        if((r = send(sockfd, buffer, bufferlen, 0)) == -1)
            return -1;
        else {
            buffer += r;
            bufferlen -= r;
            sent += r;
        }
    }

    return sent;
}

long sendstring(int sockfd, char *str)
{
    return sendbuffer(sockfd, (uint8_t *) str, strlen(str)+1);
}

long sendfile(int sockfd, char *path)
{
    struct stat s;
    stat(path, &s);

    /* First send a string containing the file name and file size */
    char str [2048];
    sprintf(str, "%s\t%lu", basename(path), s.st_size);
    
    if(sendstring(sockfd, str) == -1) {
        fprintf(stderr, "sendfile(): Error in sendstring()\n");
        return -1;
    }

    FILE *fd;
    if((fd = fopen(path, "r")) == NULL)
            return -1;

    unsigned long i = 0;
    /* Allocate 1GB for reading the file */
    uint8_t *buffer = (uint8_t *) malloc(SEN_BUFF_LEN);
    unsigned long filesize = s.st_size, r, byteread, bytesent = 0;

    while(filesize > 0) {

        /* Load file or 1GB of file into buffer */
        byteread = 0;
        unsigned long tosend = min(filesize, SEN_BUFF_LEN);
        while(byteread < tosend) {
            r = fread(buffer, sizeof(uint8_t), tosend, fd);
            filesize -= r;
            byteread += r;
        }
        
        /* Send the buffer */
        if((bytesent += sendbuffer(sockfd, buffer, byteread)) == -1) {
            fprintf(stderr, "sendfile(): Error in sendbuffer()\n");
            return -1;
        }
    }


    fclose(fd);

    return bytesent;
}
