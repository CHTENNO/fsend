#include<stdint.h>

/* Buffer size for sending is 50MB */
#define SEN_BUFF_LEN 52428800

#ifndef max
#define max(a, b) (a > b ? a : b)
#endif
#ifndef min
#define min(a, b) (a < b ? a : b)
#endif


long sendbuffer(int sockfd, uint8_t *buffer, unsigned long bufferlen);
long sendstring(int sockfd, char *str);
long sendfile(int sockfd, char *path);
