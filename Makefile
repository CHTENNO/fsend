CC = gcc
CTARGET = client/fsend_client
STARGET = server/fsend_server

all: $(CTARGET) $(STARGET)

client: $(CTARGET)

server: $(STARGET)

$(CTARGET): recv.c send.c $(CTARGET).c  
	$(CC) -g -o $(CTARGET) send.c recv.c $(CTARGET).c fsend.c -lcrypt

$(STARGET): recv.c send.c valid.c $(STARGET).c 
	$(CC) -g -o $(STARGET) send.c recv.c valid.c $(STARGET).c fsend.c -lcrypt -lpthread

clean:
	rm $(STARGET) $(CTARGET)
