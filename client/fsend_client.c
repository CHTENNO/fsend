#include<stdio.h>
#include<stdlib.h>
#include<netdb.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<string.h>
#include<stdint.h>
#include<errno.h>
#include<termios.h>
#include<libgen.h>
#include<dirent.h>
#include"../send.h"
#include"../recv.h"
#include"../fsend.h"

void usage()
{
    printf("Usage: fsend <IP Address> <Source> <Destination>\n");
    printf("-r: recursive\n");
    exit(-1);
}

int main(int argc, char *argv[])
{
    int count = argc;
    int recursive = 0;
    char **ptr = argv;
    char *addr = NULL;
    char *source = NULL;
    char *dest = NULL;

    while(--count) {
        if(*(++ptr)[0] == '-')
        switch(*++ptr[0]) {
            case 'r':
                recursive = 1;
                break;
            default:
                usage();
        }
        else {
            if(addr == NULL)
                addr = *ptr;
            else if(source == NULL)
                source = *ptr;
            else if(dest == NULL)
                dest = *ptr;
        }
    }

    if(argc < 4 || !addr || !source || !dest)
        usage();

    char username[FSEND_USERNAM_SIZE];
    char password[FSEND_PASSWRD_SIZE];

    printf("Username for %s: ", addr);
    scanf("%s", username);

    getchar();
    
    int hide_input(int fd, int opt);
    int get_password(char* pass, int size);
    printf("Password for %s@%s: ", username, addr);
    hide_input(STDIN_FILENO, 1);
    get_password(password, FSEND_PASSWRD_SIZE);
    hide_input(STDIN_FILENO, 0);

    fsend_t *handle = fsend_init();

    if(!fsend_connect(handle, addr))
        printf("Connection to fsend server failed.\n");
    else
        printf("Connected to server at %s\n", addr);

    if(fsend_client_login(handle, username, password) == -1) {
        printf("Login unsuccessful\n");
    }
    else {
        void send_dir(fsend_t *handle, char *source, char *dest);
        printf("Login successful\n");

        if(recursive)
            send_dir(handle, source, dest);
        else
            fsend_sendfile(handle, source, dest);
    }
    
    fsend_end(handle, NULL);
    return 0;
}

/*
 * Hides the input from the user when opt is set
 * to 1, and shows input when opt is otherwise.
 * */
int hide_input(int fd, int opt)
{
    struct termios term;
    tcflag_t flags = (ECHO | ECHOE | ECHOK | ECHONL);

    if(tcgetattr(fd, &term) == -1) {
        fprintf(stderr, "ERROR: Cannot get attributes of termial\n");
        return 0;
    }

    if(opt)
        term.c_lflag |= flags;
    else
        term.c_cflag &= ~flags;

    if(tcsetattr(fd, TCSAFLUSH, &term) == -1) {
        fprintf(stderr, "ERROR: Cannot set attributes of termial\n");
        return 0;
    }

    return 1;
}

/* Reads in a user password into pass
 * returns the string length of the password 
 * */
int get_password(char* pass, int size)
{
    char c;
    int i = 0;

    while(i < size-1) {
        c = getchar();
        if(c == '\n') {
            pass[i] = '\0';
            break;
        }
        pass[i++] = c;
    }

    if(i == size-1){
        pass[++i] = '\0';
    }

    return i;
}

void __send_dir(fsend_t *handle, char *source, char *dest)
{
    /* Pointer for directory */
    DIR *dp = NULL;
    /* Pointer for directory entries */
    struct dirent *dentp = NULL;

    if((dp = opendir(source)) != NULL) {
        while((dentp = readdir(dp)) != NULL) {
            if(strcmp(dentp->d_name, ".") != 0 && strcmp(dentp->d_name, "..") != 0) {
                char destdir[FSEND_FULLPATH_SIZE], sourcedir[FSEND_FULLPATH_SIZE];
                memset(destdir, 0 , FSEND_FULLPATH_SIZE);
                memset(sourcedir, 0 , FSEND_FULLPATH_SIZE);

                strcpy(destdir, dest);
                strcpy(sourcedir, source);

                if(destdir[strlen(destdir)-1] != '/')
                    strcat(destdir, "/");
                if(sourcedir[strlen(sourcedir)-1] != '/')
                    strcat(sourcedir, "/");
                
                strcat(destdir, dentp->d_name);
                strcat(sourcedir, dentp->d_name);

                if(dentp->d_type == DT_REG) {
                    printf("Sending file %s...\n", destdir);
                    fsend_sendfile(handle, sourcedir, destdir);
                }
                else if(dentp->d_type == DT_DIR) {
                    fsend_senddir(handle, destdir);
                    __send_dir(handle, sourcedir, destdir);
                }
            }
        }
    }
}

void send_dir(fsend_t *handle, char *source, char *dest)
{
    char destdir[FSEND_FULLPATH_SIZE];
    strcpy(destdir, dest);
    if(destdir[strlen(destdir)-1] != '/')
        strcat(destdir, "/");

    /* basename(3) may change it's parameter to make
     * a copy */
    char sourcecpy[FSEND_FULLPATH_SIZE];
    strcpy(sourcecpy, source);
    strcpy(destdir, basename(sourcecpy));

    fsend_senddir(handle, destdir);
    __send_dir(handle, source, destdir);
}
