#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<arpa/inet.h>
#include<stdarg.h>
#include<crypt.h>
#include<pwd.h>
#include<shadow.h>
#include<libgen.h>
#include<errno.h>
#include"fsend.h"
#include"send.h"
#include"recv.h"

/* Initalises an fsend handle
 */
fsend_t *fsend_init()
{
    fsend_t *handle = (fsend_t *) malloc(sizeof(fsend_t));
    handle->login = 0;
    handle->sockfd = -1;
    return handle;
}

/* Gets the line specified in line from the body of the fsend_request */
void __fsend_getline(struct fsend_request *req, int lineno, char *line, int size)
{
    char body[FSEND_BODY_SIZE];
    char *str;
    memcpy(body, req->body, FSEND_BODY_SIZE);
    char *ptr = body;
    
    /* Get first token*/
    str = strtok(body, "\r\n");
    
    for(int ln = 1; ln < lineno; ln++) {
        /* Get next tokens */
        str = strtok(NULL, "\r\n");
    }

    /* If str is NULL then the body only contained the terminator
     * for an fsend_request an was empty */
    if(str != NULL)
        strcpy(line, str);
    else
        strcpy(line, "FSEND_REQUEST BODY EMPTY");
}

/* Connects to an fsend server and sets the sockfd in handle to the
 * new connection, returns 1 on success and 0 on error
 */
int fsend_connect(fsend_t *handle, char *address)
{
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_port = htons(FSEND_PORT);
    addr.sin_family = AF_INET;

    if(!inet_pton(AF_INET, address, &addr.sin_addr.s_addr))
        return 0;

    strcpy(handle->addr, address);

    int fd;
    if((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        return 0;

    if(connect(fd, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1)
        return 0;

    handle->sockfd = fd;
    return 1;
}

/* Disconnects from the connected fsend server and sets the sockfd to
 * -1
 */
int fsend_disconnect(fsend_t *handle)
{
    if(!connected(handle))
        return 0;

    close(handle->sockfd);
    printf("Closed connection: %s\n", handle->addr);
    handle->sockfd = -1;
    strcpy(handle->addr, "");
    return 1;
}

/* Sends the fsend structure in req via the socket in handle
 * returns the number of bytes sent (which should be 2048), 
 * 0 if there is no connection, -1 on error. 
 */
int send_fsend_req(fsend_t *handle, struct fsend_request *req)
{
    if(!connected(handle))
        return 0;

    return sendbuffer(handle->sockfd, (uint8_t *) req, sizeof(struct fsend_request));
}

/* Receives an fsend structure into req via the socket in handle
 * returns the number of bytes received (which should be 2048), 
 * 0 if there is no connection, -1 on error. 
 */
int recv_fsend_req(fsend_t *handle, struct fsend_request *req)
{
    if(!connected(handle))
        return 0;
    return recvbuffer(handle->sockfd, (uint8_t *) req, sizeof(struct fsend_request));
}

/* Sends a FSEND_ACK message with the specified body (can be NULL)
 * returns 1 on success and -1 on error
 */
int send_fsend_ack(fsend_t *handle, char *body)
{
    void fsend_package(struct fsend_request *req, uint8_t code, int lines, ...);
    struct fsend_request req;
    fsend_package(&req, FSEND_ACK, (body == NULL ? 0 : 1), body);
    if(send_fsend_req(handle, &req) == -1)
        return -1;

    return 1;
}

/* Sends a FSEND_ERR message with the specified body (can be NULL)
 * returns 1 on success and -1 on error
 */
int send_fsend_err(fsend_t *handle, char *body)
{
    void fsend_package(struct fsend_request *req, uint8_t code, int lines, ...);
    struct fsend_request req;
    fsend_package(&req, FSEND_ERR, (body == NULL ? 0 : 1), body);
    if(send_fsend_req(handle, &req) == -1)
        return -1;

    return 1;
}

/* Receives an FSEND_ACK message and saves the body (body can be NULL) 
 * returns 1 is FSEND_ACK was received succesfully, -1 if FSEND_ERR was
 * received and 0 if the message was not a valid FSEND_ACK or FSEND_ERR
 * message
 */
int recv_fsend_ack(fsend_t *handle, char *body, int size)
{
    struct fsend_request req;

    if(recv_fsend_req(handle, &req) <= 0)
        return -1;

    int i = 0;
    if(req.code == FSEND_ERR) {
        if(body != NULL)
            __fsend_getline(&req, 1, body, size);
        return -1;
    }
    else if(req.code == FSEND_ACK) {
        if(body != NULL) 
            __fsend_getline(&req, 1, body, size);
        return 1;
    }
    else
        return 0;
}

/* Ends fsend session from client by sending FSEND_END to the server
 */
int fsend_end(fsend_t *handle, char *body)
{
    void fsend_package(struct fsend_request *req, uint8_t code, int lines, ...);
    struct fsend_request req;
    fsend_package(&req, FSEND_END, (body == NULL ? 0 : 1), body);

    int r = 1;

    if(send_fsend_req(handle, &req) == -1)
        r = -1;

    fsend_disconnect(handle);
    return r;
}

/* Packages an struct fsend_request into req using the specified code and a
 * variable length of lines. Each line is terminated with a '\r'\n' except
 * for the end of the request which is terminated with '\r\n\r\n'
 */
void fsend_package(struct fsend_request *req, uint8_t code, int lines, ...)
{
    memset(req, 0, sizeof(struct fsend_request));
    strncpy((char *)req->preamble, "FSEND_REQ", 9);
    req->code = code;
    uint8_t *ptr = req->body;

    if(lines > 0) {
        va_list ap;
        char *line;
        int len;

        va_start(ap, lines);

        for(int i = 1; i < lines; i++) {
            line = va_arg(ap, char *);
            len = strlen(line);
            strncpy((char *)ptr, line, len);
            ptr += len;
            strncpy((char *)ptr, "\r\n", 2);
            ptr += 2;
        }

    
        line = va_arg(ap, char *);
        len = strlen(line);
        strncpy((char *)ptr, line, len);
        ptr += len;
    }
    strncpy((char *)ptr, "\r\n\r\n", 4);
}

/* Sends the file specified in path to the sockfd in handle
 */
long fsend_sendfile(fsend_t *handle, char* source, char *dest)
{
    struct stat s;

    /* First check that the source file exists and is a file
     * and not a directory */
    if(stat(source, &s) != -1) {
        if(S_ISDIR(s.st_mode))
            return -1;
    }
    else
        return -1;

    /* Create FSEND_SFILE request */
    struct fsend_request req;
    char s_filesize[FSEND_FILESSTR_SIZE];
    int response;

    sprintf(s_filesize, "%lu", s.st_size);

    /* basename(3) may change content of it's
     * parameter so create a copy */
    char sourcecpy[FSEND_FILENAME_SIZE];
    strcpy(sourcecpy, source);
    char *filename = basename(sourcecpy);

    /* FSEND_SFILE request contains:
     * 1st line: filename
     * 2nd line: destination path
     * 3rd line: filesize
     */
    fsend_package(&req, FSEND_SFILE, 3,
            filename,
            dest,
            s_filesize);

    if(send_fsend_req(handle, &req) <= 0)
        return -1;

    /* Buffer for message in FSEND_ACK or FSEND_ERR message */
    char fsend_buffer[FSEND_BODY_SIZE];
    response = recv_fsend_ack(handle, fsend_buffer, FSEND_BODY_SIZE);

    /* If recv_fsend_ack returned -1 then FSEND_ERR was received */
    if(response == -1) {
        fprintf(stderr, "Received FSEND_ERR: %s\n", fsend_buffer);
        return -1;
    }

    FILE *fd;
    if((fd = fopen(source, "r")) == NULL)
            return -1;

    unsigned long i = 0;
    /* Allocate buffer for reading the file */
    uint8_t *buffer = (uint8_t *) malloc(SEN_BUFF_LEN);
    unsigned long filesize = s.st_size, r, byteread, bytesent = 0;

    while(filesize > 0) {
        /* Load file or BUFFSIZE of file into buffer */
        byteread = 0;
        unsigned long tosend = min(filesize, SEN_BUFF_LEN);
        while(byteread < tosend) {
            r = fread(buffer, sizeof(uint8_t), tosend, fd);
            filesize -= r;
            byteread += r;
        }
        
        /* Send the buffer */
        if((bytesent += sendbuffer(handle->sockfd, buffer, byteread)) == -1) {
            fprintf(stderr, "sendfile(): Error in sendbuffer()\n");
            return -1;
        }

        /* Save cursor position */
        fputs("\0337", stdout);
        float percent = ((float)bytesent / s.st_size) * 100; 
        printf("\r%s: sent %lu/%lu bytes... %.0f%%", source, bytesent, s.st_size, percent);
        /* Restore cursor position */
        fputs("\0338", stdout);
        fflush(stdout);
    }

    printf("\n");

    fclose(fd);
    free(buffer);

    /* Receive FSEND_ACK to acknowledge that file has been received succesfully */
    response = recv_fsend_ack(handle, fsend_buffer, FSEND_BODY_SIZE);

    /* If recv_fsend_ack returned -1 then FSEND_ERR was received */
    if(response == -1) {
        fprintf(stderr, "Received FSEND_ERR: %s\n", fsend_buffer);
        return -1;
    }

    return bytesent;
}

long fsend_recvfile(fsend_t *handle, struct fsend_request *req)
{
    /* First get the filename/path and file size from the FSEND_SFILE 
     * request */
    char filename[FSEND_FILENAME_SIZE];
    char fullpath[FSEND_FULLPATH_SIZE];
    char filesizestr[FSEND_FILESSTR_SIZE];
    /* Used for error message */
    char str[FSEND_BODY_SIZE];

    __fsend_getline(req, 1, filename, FSEND_FILENAME_SIZE);
    __fsend_getline(req, 2, fullpath, FSEND_FULLPATH_SIZE);
    __fsend_getline(req, 3, filesizestr, FSEND_FILESSTR_SIZE);

    /* dirname may change it's paramater so make a copy */
    char fullpathcopy[FSEND_FILENAME_SIZE];
    strcpy(fullpathcopy, fullpath);
    
    /* First check is the path exists */
    struct stat s;
    if(stat(fullpath, &s) != -1) {
        /* If the full path is a directory then write the file
         * as the given filename */
        if(S_ISDIR(s.st_mode)) {
            if(fullpath[strlen(fullpath)-1] != '/')
                strcat(fullpath, "/");
            strcat(fullpath, filename);
        }
    }
    /* If stat returned -1 then either:
     * 1. The path is valid and the file doesn't exist yet
     * 2. The path is not valid
     * So stat the dirname of full path to check if it exists. */
    else {
        char *dirnamestr = dirname(fullpathcopy);
        if (stat(dirnamestr, &s) == -1) {
            /* Client expects an acknowledgement of the request so send an error message to the client */
            sprintf(str, "%s: %s", dirnamestr, strerror(errno));
            send_fsend_err(handle, str);
            return -1;
        }
    }

    /* Send FSEND_ACK to acknowledge recipt of request */
    send_fsend_ack(handle, NULL);
    
    unsigned long filesize = atol(filesizestr);

    uint8_t *buffer = (uint8_t*) malloc(RECV_BUFF_LEN);
    
    FILE *fd;
    if((fd = fopen(fullpath, "w")) == NULL) {
        sprintf(str, "Couldn't open %s for writing: %s", 
                fullpath, strerror(errno));
        send_fsend_err(handle, str);
        return -1;
    }

    unsigned long fsize = filesize, r, byteswriten = 0;

    while(byteswriten < filesize) {
        if((r = recvbuffer(handle->sockfd, buffer, min(RECV_BUFF_LEN, fsize))) <= 0) {
            remove(fullpath);
            sprintf(str, "%s: %s", fullpath, strerror(errno));
            send_fsend_err(handle, str);
            return r;
        }

        r = fwrite(buffer, sizeof(uint8_t), r, fd);
        fsize -= r;
        byteswriten += r;


        /* Save cursor position */
        fputs("\0337", stdout);
        float percent = ((float)byteswriten / filesize) * 100; 
        printf("\r%s: received %lu/%lu bytes... %.0f%%", filename, byteswriten, filesize, percent);
        /* Restore cursor position */
        fputs("\0338", stdout);
        fflush(stdout);
    }

    printf("\n");

    fclose(fd);
    free(buffer);

    if(chown(fullpath, handle->uid, handle->gid) == -1) {
        send_fsend_err(handle, "Couldn't change ownership to user\n");
        return -1;
    }

    /* Send FSEND_ACK to indicate sucessful reception of file */
    send_fsend_ack(handle, "File received.");

    printf("Received %lu byte file from %s: %s\n", byteswriten, handle->addr, filename);
    return byteswriten;
}

/* Logs the client in on the fsend server. Returns 1 on success, -1 on error */
int fsend_client_login(fsend_t *handle, char *username, char *password)
{
    struct fsend_request req;
    /* Packed login request, lines of login request are:
     * Line 1: username
     * Line 2: password
     */
    fsend_package(&req, FSEND_LOGIN, 2,
            username,
            password);

    /* Send login request */
    if(send_fsend_req(handle, &req) == -1)
        return -1;

    /* Receive acknowledgement that login was successful. If
     * FSEND_ERR was received then login was not successful
     */
    char body[FSEND_BODY_SIZE];
    int result = recv_fsend_ack(handle, body, FSEND_BODY_SIZE);

    if(result == -1)
        fprintf(stderr, "Login unsuccessful: %s\n", body);
    else if(result)
        handle->login = 1;
    
    return result;
}

/* Send information about a directory to the server returns
 * 1 on success and -1 on error */
int fsend_senddir(fsend_t *handle, char *dest)
{
    int response;

    struct fsend_request req;

    /* FSEND_SDIR request contains:
     * 1st line: directory name
     */
    fsend_package(&req, FSEND_SDIR, 1,
            dest);

    /* Recive acknowledgement that directory was received
     * and created succesfully */
    if(send_fsend_req(handle, &req) <= 0)
        return -1;

    /* Buffer for message in FSEND_ACK or FSEND_ERR message */
    char fsend_buffer[FSEND_BODY_SIZE];
    response = recv_fsend_ack(handle, fsend_buffer, FSEND_BODY_SIZE);

    /* If recv_fsend_ack returned -1 then FSEND_ERR was received */
    if(response == -1) {
        fprintf(stderr, "Received FSEND_ERR: %s\n", fsend_buffer);
        return -1;
    }

    return 1;
}

/* Receives information about a directory from the client and creates said
 * directory. Returns 1 on success and sends FSEND_ACK, or returns -1 on error
 * and sends FSEND_ERR */
int fsend_recvdir(fsend_t *handle, struct fsend_request *req)
{
    struct stat st;
    char dir[FSEND_FILENAME_SIZE];
    __fsend_getline(req, 1, dir, FSEND_FILENAME_SIZE);

    /* First check if directory exists */
    if(stat(dir, &st) == -1) {
        /* If it doesn't exist create it */
        if(mkdir(dir, 0755) == -1) {
            send_fsend_err(handle, "Cannot create directory");
            fprintf(stderr, "%s Cannot create directory %s: %s\n", handle->addr, dir, strerror(errno));
            return -1;
        }
        else {
            printf("Created directory %s: %s\n", handle->addr, dir);
            if(chown(dir, handle->uid, handle->gid) == -1) {
                send_fsend_err(handle, "Couldn't change ownership to user\n");
                fprintf(stderr, "%s cannot chown directory %s\n", handle->addr, dir);
                return -1;
            }
        }
    }

    send_fsend_ack(handle, "Directory received");
    return 1;
}

int fsend_server_login(fsend_t *handle, struct fsend_request *req)
{
    char username[FSEND_USERNAM_SIZE];
    char password[FSEND_PASSWRD_SIZE];

    __fsend_getline(req, 1 , username, FSEND_USERNAM_SIZE);
    __fsend_getline(req, 2 , password, FSEND_PASSWRD_SIZE);

    struct passwd *user_pass = getpwnam(username);

    /* If user_pass is NULL then trhe username doesn't exist in the
     * /etc/passwd file */
    if(!user_pass) {
        fprintf(stderr, "Error: %s doesn't exist on system\n", username);
        send_fsend_err(handle, "User doesn't exist on system");
        return -1;
    }

    int result = 0;

    /* If the password is "x" then the password is encrypted and located in
     * the /etc/shadow file */
    if(!strcmp(user_pass->pw_passwd, "x")) {
        struct spwd *user_shadow = getspnam(username);
        if(!user_shadow) {
            fprintf(stderr, "Error: %s doesn't exist in shadow file\n", username);
            send_fsend_err(handle, "User doesn't exist in shadow file");
            return -1;
        }

        if(!strcmp(user_shadow->sp_pwdp, crypt(password, user_shadow->sp_pwdp)))
            result = 1;
    }
    else
        if(!strcmp(user_pass->pw_passwd, crypt(password, user_pass->pw_passwd)))
            result = 1;
        
    /* If credentials match the credentials on the system */
    if(result) {
        handle->uid = user_pass->pw_uid;
        handle->gid = user_pass->pw_gid;
        strcpy(handle->username, username);
        strcpy(handle->password, password);
        handle->login = 1;
        send_fsend_ack(handle, "Login successful");
    }
    else {
        send_fsend_err(handle, "Incorrect password");
    }

    return result;
}

void fsend_handle_req(fsend_t *handle, struct fsend_request *req)
{
    switch(req->code) {
        case FSEND_INIT:
            break;

        case FSEND_LOGIN:
            if(fsend_server_login(handle, req) == 1)
                printf("Connection on socket %s: Login successful!\n", handle->addr);
            else
            {
                printf("Connection on socket %s: Login unsuccessful\n", handle->addr);
                fsend_disconnect(handle);
            }
            break;

        case FSEND_SFILE:
            if(loggedin(handle)) {
                if(fsend_recvfile(handle, req) == -1) {
                    fprintf(stderr, "\n%s: Error receiving file - %s\n", handle->addr, strerror(errno));
                    fsend_disconnect(handle);
                }
            }
            else
                fprintf(stderr, "Received FSEND_SFILE but client it not logged in\n");
            break;

        case FSEND_SDIR:
            if(loggedin(handle)) {
                fsend_recvdir(handle, req);
            }
            else
                fprintf(stderr, "Received FSEND_SDIR but client it not logged in\n");
            break;

        case FSEND_END:
            fsend_disconnect(handle);
            break;
    }
}
